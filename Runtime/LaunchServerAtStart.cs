﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchServerAtStart : MonoBehaviour
{
    public UserMQTT m_server;
    public IpPortKeyboard m_keyboard;

    private void Start()
    {
        m_server.m_serverIp = m_keyboard.GetIpAsString(false);
        m_server.m_serverPort = m_keyboard.GetPort();
        m_server.StartServerListener();
    }
}
