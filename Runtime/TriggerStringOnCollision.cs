﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerStringOnCollision : MonoBehaviour
{
    public string m_triggered;
    public StringTriggeredEvent m_onEnter;

    private void OnTriggerEnter(Collider other)
    {
        TriggerAssociatedString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        TriggerAssociatedString();

    }

    public void TriggerAssociatedString() {
        m_onEnter.Invoke(m_triggered);
    }

    [System.Serializable]
    public class StringTriggeredEvent : UnityEvent<string> { }
}
