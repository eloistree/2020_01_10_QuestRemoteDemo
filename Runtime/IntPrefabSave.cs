﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntPrefabSave : MonoBehaviour
{
    public IntPrefab m_toAffect;
    public string m_randomPrefId;

    void Awake()
    {
       if( PlayerPrefs.HasKey(m_randomPrefId))
        m_toAffect.SetValue(PlayerPrefs.GetInt( m_randomPrefId));
    }
    
    void OnDisable()
    {

        PlayerPrefs.SetInt( m_randomPrefId, m_toAffect.GetValue()) ;

    }
    private void Reset()
    {
        m_toAffect = GetComponent<IntPrefab>();
        m_randomPrefId = ""+Random.Range(0, int.MaxValue);
    }
}
