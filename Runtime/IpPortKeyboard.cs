﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IpPortKeyboard : MonoBehaviour
{
    public IntPrefab ipv4_0;
    public IntPrefab ipv4_1;
    public IntPrefab ipv4_2;
    public IntPrefab ipv4_3;
    public IntPrefab ipv4_port;


    public string GetIpAsString(bool withPort)
    {
        string ip = string.Format("{0}.{1}.{2}.{3}",
            ipv4_0.GetValue(), ipv4_1.GetValue(), ipv4_2.GetValue(), ipv4_3.GetValue());
        if (withPort)
            ip += ":" + ipv4_port.GetValue();
        return ip;
    }
    public string GetPortAsString()
    {
        return ipv4_port.ToString();
    }
    public int GetPort()
    {
        return ipv4_port.GetValue();
    }
}
