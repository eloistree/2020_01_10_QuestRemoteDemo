﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HandsReplayMovement : MonoBehaviour
{
    public TextAsset m_movementToReplay;
    public HandsMovementAsKeys keys;
    public QuestHandsStateTransformMono m_display;
    public QuestHandsStateTransform.HandsTransformDisplayType relocationType;
    public int m_maxKey;
    public float m_duration;
    public float m_timePass;
    


    // Start is called before the first frame update
    void Start()
    {
        keys = new HandsMovementAsKeys( OculusHandsRecordAsString.GetKeys(m_movementToReplay.text));
        keys.RemoveTimeAtStart();
        keys.Order();
        m_duration = keys.GetTotalTime();
        m_maxKey = keys.GetCount();
    }

    // Update is called once per frame
    void Update()
    {
        m_timePass += Time.deltaTime;
        HandsRecordKey key = null;

        if (keys.GetNextKey(m_timePass % m_duration, out key)) {

            if (m_display)
            {
               QuestHandsStateKey state= QuestHandsUtility.ReconstructStateFromCompressedString(key.m_compressedVersion); 
               m_display.GetTransformState().SetFromDataToTransform(state, relocationType);
            }
        }
     
    }
    
}
