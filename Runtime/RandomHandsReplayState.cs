﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomHandsReplayState : MonoBehaviour
{
    public TextAsset [] m_handsStateAsFile;
    public QuestHandsStateTransformMono m_display;
    public QuestHandsStateTransform.HandsTransformDisplayType relocationType;
    public float m_timeBetweenSwitch=0.5f;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_timeBetweenSwitch);
            if (m_handsStateAsFile.Length > 0) {
                m_display.ReconstructFromStringVersion(GetRandom(), relocationType);
            }

        }
        
    }

    private string GetRandom()
    {
        return m_handsStateAsFile[UnityEngine.Random.Range(0, m_handsStateAsFile.Length)].text;
    }
    
}
