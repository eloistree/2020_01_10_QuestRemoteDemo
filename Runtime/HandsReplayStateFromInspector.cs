﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandsReplayStateFromInspector : MonoBehaviour
{
    [Header("To Replay")]
    public TextAsset m_compressVersionAsFile;
    public string m_compressVersion;

    public QuestHandsStateTransformMono m_handDebug;
    public QuestHandsStateTransform.HandsTransformDisplayType relocationType;

   
    public void OnValidate()
    {
        if (m_compressVersionAsFile != null)
        {
               m_compressVersion = m_compressVersionAsFile.text;
            m_compressVersionAsFile = null;

        }
        if (!string.IsNullOrEmpty(m_compressVersion) )
        {
            m_handDebug.ReconstructFromStringVersion(m_compressVersion, relocationType);
            m_compressVersion = "";

        }

    }
}
