﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IntPrefab : MonoBehaviour
{
    public int m_value;
    public int m_min, m_max=9999;
    public OnValueChangeInt m_onChangeAsInt;

    public void SetValue(int value)
    {
        m_value = value;
        Clamp();
        Refresh();
    }

    public OnValueChangeString m_onChangeAsString;

    public void Start()
    {
        Refresh();
    }

    public void AddValue(int value) {
       SetValue(GetValue() + value);
    }
    public void RemoveValue(int value)
    {
        SetValue(GetValue() - value);
    }

    public int GetValue()
    {
        return m_value;
    }

    private void Refresh()
    {
        m_onChangeAsInt.Invoke(m_value);
        m_onChangeAsString.Invoke(m_value.ToString());
    }



    private void Clamp()
    {
        m_value = Mathf.Clamp(m_value, m_min, m_max);

    }
    [System.Serializable]
    public class OnValueChangeInt : UnityEvent<int > { }
    [System.Serializable]
    public class OnValueChangeString : UnityEvent<string> { }
}
